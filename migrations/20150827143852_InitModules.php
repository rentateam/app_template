<?php 
class InitModules extends \Project\Migration
{
    /**
    * Do the migration
    */
    public function up()
    {
        $db = $this->app->getSFM()->getDb();

        $db->query("
            CREATE TABLE `Privilege` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                `code` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE INDEX `code` (`code`)
            )
            ENGINE=InnoDB;
        ");

        $db->query("
            CREATE TABLE `AccountRole` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                `code` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`),
                UNIQUE INDEX `code` (`code`)
            )
            ENGINE=InnoDB
            AUTO_INCREMENT=1;
        ");

        $db->query("
            CREATE TABLE `Account` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `email` VARCHAR(255) NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                `password` VARCHAR(255) NOT NULL,
                `salt` VARCHAR(255) NOT NULL,
                `account_role_id` INT(10) UNSIGNED NOT NULL,
                `is_deleted` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
                PRIMARY KEY (`id`),
                INDEX `FK_Account_AccountRole` (`account_role_id`),
                CONSTRAINT `FK_Account_AccountRole` FOREIGN KEY (`account_role_id`) REFERENCES `AccountRole` (`id`)
            )
            ENGINE=InnoDB
            AUTO_INCREMENT=1;
        ");

        $db->query("
            CREATE TABLE `AccountRoleToPrivilege` (
                `account_role_id` INT(10) UNSIGNED NOT NULL,
                `privilege_id` INT(10) UNSIGNED NOT NULL,
                UNIQUE INDEX `account_role_ie_privilege_id` (`account_role_id`, `privilege_id`),
                INDEX `FK_AccountRoleToPrivilege_Privilege` (`privilege_id`),
                CONSTRAINT `FK_AccountRoleToPrivilege_AccountRole` FOREIGN KEY (`account_role_id`) REFERENCES `AccountRole` (`id`),
                CONSTRAINT `FK_AccountRoleToPrivilege_Privilege` FOREIGN KEY (`privilege_id`) REFERENCES `Privilege` (`id`)
            )
            ENGINE=InnoDB;
        ");

        $db->query("
          INSERT INTO `AccountRole` (`id`, `name`, `code`) VALUES (1, 'Admin', 'ROLE_ADMIN');
        ");

        $db->query("
          INSERT INTO `Privilege` (`id`, `name`, `code`) VALUES (1, 'Edit accounts', 'ROLE_EDIT_ACCOUNTS');
          INSERT INTO `Privilege` (`id`, `name`, `code`) VALUES (2, 'View logs', 'ROLE_VIEW_LOGS');
        ");

        $db->query("
          INSERT INTO `AccountRoleToPrivilege` (`account_role_id`, `privilege_id`) VALUES (1, 1);
          INSERT INTO `AccountRoleToPrivilege` (`account_role_id`, `privilege_id`) VALUES (1, 2);
        ");

        $db->query("
            INSERT INTO `Account` (`id`, `email`, `name`, `password`, `salt`, `account_role_id`, `is_deleted`) VALUES (1, 'raccoon@rentateam.ru', 'Raccoon Adventurer', 'ptDbLJn5FDDNgtbBQP57C3SoYns8v0Y1tsKY3sWtqRj7WQMMs8ndUAMpaLmP3EjygfKIG87dYNaGsutVJ4hctA==', '8ebc23b4535a6894b18cf958d637ce49', 1, 0);
        ");

        $db->query("
            CREATE TABLE `LogAction` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            AUTO_INCREMENT=1;
        ");

        $db->query("
            CREATE TABLE `LogDiffSet` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                PRIMARY KEY (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            AUTO_INCREMENT=1;
        ");

        $db->query("
            CREATE TABLE `LogDiff` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `log_diffset_id` INT(10) UNSIGNED NOT NULL,
                `field` VARCHAR(255) NOT NULL,
                `before` MEDIUMTEXT NULL,
                `after` MEDIUMTEXT NULL,
                PRIMARY KEY (`id`),
                INDEX `FK_LogDiff_LogDiffSet` (`log_diffset_id`),
                CONSTRAINT `FK_LogDiff_LogDiffSet` FOREIGN KEY (`log_diffset_id`) REFERENCES `LogDiffSet` (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB
            AUTO_INCREMENT=1;
        ");

        $db->query("
            CREATE TABLE `AccountLog` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `account_id` INT(10) UNSIGNED NULL DEFAULT NULL,
                `log_action_id` INT(10) UNSIGNED NOT NULL,
                `entity_id` INT(10) UNSIGNED NOT NULL,
                `log_diffset_id` INT(10) UNSIGNED NULL DEFAULT NULL,
                `datetime` DATETIME NOT NULL,
                PRIMARY KEY (`id`),
                INDEX `AccountLog_Account` (`account_id`),
                INDEX `AccountLog_Entity` (`entity_id`),
                INDEX `AccountLog_LogAction` (`log_action_id`),
                INDEX `AccountLog_LogDiff` (`log_diffset_id`),
                INDEX `AccountLog_Datetime` (`datetime`),
                CONSTRAINT `FK_AccountLog_Account` FOREIGN KEY (`account_id`) REFERENCES `Account` (`id`),
                CONSTRAINT `FK_AccountLog_Entity` FOREIGN KEY (`entity_id`) REFERENCES `Account` (`id`),
                CONSTRAINT `FK_AccountLog_LogAction` FOREIGN KEY (`log_action_id`) REFERENCES `LogAction` (`id`),
                CONSTRAINT `FK_AccountLog_LogDiffSet` FOREIGN KEY (`log_diffset_id`) REFERENCES `LogDiffSet` (`id`)
            )
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB;
        ");

        $db->query("
            INSERT INTO `LogAction` (`id`, `name`) VALUES (2, 'ACCOUNT_UPDATED');
        ");

        $db->query("
            INSERT INTO `LogAction` (`id`, `name`) VALUES (1, 'ACCOUNT_CREATED');
        ");

        $db->query("
            INSERT INTO `LogAction` (`id`, `name`) VALUES (3, 'ACCOUNT_PASSWORD_CHANGED');
        ");

        $db->query("
            INSERT INTO `LogAction` (`id`, `name`) VALUES (4, 'ACCOUNT_DELETED');
        ");

        $db->query("
            INSERT INTO `LogAction` (`id`, `name`) VALUES (5, 'ACCOUNT_RESTORED');
        ");

    }

    /**
    * Undo the migration
    */
    public function down()
    {

    }
}