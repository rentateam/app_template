<?php
require_once 'vendor/autoload.php';

$app = new Project\Application();

$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Simblo\Silex\Provider\EnvironmentServiceProvider([
    'variable' => 'APP_ENVIRONMENT',
    'environments' => ['production', 'testing', 'development']
]));
$app->register(new Silex\Provider\TranslationServiceProvider(), [
    'translator.messages' => [],
    'translator.domains' => [],
    'locale' => 'en',
    'locale_fallbacks' => ['en']
]);
$app->register(new Igorw\Silex\ConfigServiceProvider(getcwd() . "/config/{$app->getEnvironment()}.yml", [], new Astartsky\InheritanceConfigDriver('config/')));
$app->register(new SFM\Silex\SfmServiceProvider());
$app->register(new \Silex\Provider\SecurityServiceProvider(), [
    'security.firewalls' => [
        'login' => [
            'pattern' => '^/admin/login$',
            'anonymous' => true
        ],
        'main' => [
            'pattern' => '^.*$',
            'anonymous' => false,
            'form' => [
                'login_path' => '/admin/login',
                'check_path' => '/admin/login_check',
                'default_target_path' => '/admin'
            ],
            'logout' => [
                'logout_path' => '/admin/logout'
            ],
            'users' => $app->share(function () use ($app) {
                return $app->getAccountProvider();
            })
        ]
    ]
]);
$app->register(new Knp\Provider\ConsoleServiceProvider());
$app->register(new Project\Module\SystemModule\CronSystemProvider());
$app->register(new Project\Module\SystemModule\SystemServiceProvider());
$app->register(new Project\Module\AdminLogModule\AdminLogServiceProvider());
$app->register(new Project\Module\AccountModule\AccountServiceProvider());
$app->register(new Project\Module\AdminAccountModule\AdminAccountServiceProvider());

return $app;