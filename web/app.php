<?php
chdir(__DIR__ . "/..");

/** @var \Project\Application $app */
$app = require_once("bootstrap.php");
$app->run();