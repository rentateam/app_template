$(document).ready(function () {

    var $body = $('body');

    $body.on('ACCOUNT_DELETE.STARTED', function() {
        $('[data-action="account-delete"]').attr('disabled', 'disabled');
    });

    $body.on('ACCOUNT_DELETE.FINISHED', function() {
        $('[data-action="account-delete"]').removeAttr('disabled');
    });

    $body.on('ACCOUNT_DELETE.ERROR', function(e, message) {
        var $modal = $('#modal_delete');
        var $errorContent = $modal.find('[data-modal="error-content"]');
        var $error = $modal.find('[data-modal="error"]');
        $errorContent.html(message);
        $error.promise().done(function () {
            $error.slideDown();
        });
    });

    $body.on('ACCOUNT_RESTORE.STARTED', function() {
        $('[data-action="account-restore"]').attr('disabled', 'disabled');
    });

    $body.on('ACCOUNT_RESTORE.FINISHED', function() {
        $('[data-action="account-restore"]').removeAttr('disabled');
    });

    $body.on('ACCOUNT_RESTORE.ERROR', function(e, message) {
        var $modal = $('#modal_restore');
        var $errorContent = $modal.find('[data-modal="error-content"]');
        var $error = $modal.find('[data-modal="error"]');
        $errorContent.html(message);
        $error.promise().done(function () {
            $error.slideDown();
        });
    });

    $body.on('click', '[data-action-account-delete]', function (e) {
        e.preventDefault();

        $.ajax({
            type: "DELETE",
            url: $(this).attr('data-action-account-delete'),
            dataType: 'json',
            beforeSend: function (xhr) {
                $('body').trigger('ACCOUNT_DELETE.STARTED');
            }
        }).error(function (xhr, type, message) {
            $('body').trigger('ACCOUNT_DELETE.ERROR', message);
        }).done(function (data) {
            if (data.status == 'ok') {
                location.reload();
            }
        }).always(function() {
            $('body').trigger('ACCOUNT_DELETE.FINISHED');
        });
    });

    $body.on('click', '[data-action-account-restore]', function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: $(this).attr('data-action-account-restore'),
            dataType: 'json',
            beforeSend: function (xhr) {
                $('body').trigger('ACCOUNT_RESTORE.STARTED');
            }
        }).error(function (xhr, type, message) {
            $('body').trigger('ACCOUNT_RESTORE.ERROR', message);
        }).done(function (data) {
            if (data.status == 'ok') {
                location.reload();
            }
        }).always(function() {
            $('body').trigger('ACCOUNT_RESTORE.FINISHED');
        });
    });

    $('#modal_delete').on('show.bs.modal', function (e) {
        var $modal = $(this);
        var $content = $modal.find('[data-modal="content"]');
        var text = Mustache.render($content.attr('data-mustache'), {
                email: e.relatedTarget.getAttribute('data-email')}
        );
        $content.html(text);

        var href = e.relatedTarget.getAttribute('data-href');
        $modal.find('[data-action-account-delete]').attr('data-action-account-delete', href);
    });

    $('#modal_restore').on('show.bs.modal', function (e) {
        var $modal = $(this);
        var $content = $modal.find('[data-modal="content"]');
        var text = Mustache.render($content.attr('data-mustache'), {
                email: e.relatedTarget.getAttribute('data-email')}
        );
        $content.html(text);

        var href = e.relatedTarget.getAttribute('data-href');
        $modal.find('[data-action-account-restore]').attr('data-action-account-restore', href);
    });
});