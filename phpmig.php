<?php
/** @var \Project\Application $app */
$app = require_once("bootstrap.php");

$container = new ArrayObject();
$container['app'] = $app;
$container['phpmig.migrations_template_path'] = __DIR__ . DIRECTORY_SEPARATOR .'view/migration.php';
$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';
$container['phpmig.adapter'] = new \Astartsky\Phpmig\ZendDb2Adapter($app->getSFM()->getDb()->getAdapter(), 'Migration');

return $container;