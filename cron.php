<?php

use Cron\Cron;
use Cron\Executor\Executor;
use Cron\Report\CronReport;
use Cron\Report\JobReport;
use Cron\Resolver\ArrayResolver;
use Project\Application;

set_time_limit(0);
chdir(__DIR__);

/** @var Application $app */
$app = require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

/** @var ArrayResolver $resolver */
$resolver = $app['cron'];

$cron = new Cron();
$cron->setExecutor(new Executor());
$cron->setResolver($resolver);

/** @var CronReport $cronReport */
$cronReport = $cron->run();

echo "Running cron tasks...\n";

/** @var JobReport $report */
foreach ($cronReport->getReports() as $jobReport) {
    $output = count($jobReport->getError()) > 0 ? $jobReport->getError() : $jobReport->getOutput();
    echo implode("\n", $output);
}

echo "Completed.\n";