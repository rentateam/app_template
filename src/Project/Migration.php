<?php
namespace Project;

abstract class Migration extends \Phpmig\Migration\Migration
{
    /** @var Application */
    protected $app;

    public function init()
    {
        $this->app = $this->getContainer()['app'];
    }
}