<?php
namespace Project;

use Project\Module\AccountModule\AccountTrait;
use Project\Module\AdminAccountModule\AdminAccountTrait;
use Project\Module\AdminLogModule\AdminLogTrait;
use Project\Module\SystemModule\DatabaseTrait;
use SFM\Silex\SfmAppTrait;
use Project\Module\SystemModule\SystemTrait;

class Application extends \Silex\Application
{
    use SfmAppTrait;
    use SystemTrait;
    use DatabaseTrait;
    use AccountTrait;
    use AdminAccountTrait;
    use AdminLogTrait;

    /**
     * @return string
     */
    public function getEnvironment()
    {
        return $this['environment'];
    }
}