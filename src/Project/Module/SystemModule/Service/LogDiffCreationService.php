<?php
namespace Project\Module\SystemModule\Service;

use Diff\Differ\MapDiffer;
use Diff\DiffOp\DiffOp;
use Utility\ObjectToArray;

class LogDiffCreationService
{
    protected $mapperLogDiffSet;
    protected $mapperLogDiff;

    /**
     * @param \Mapper_LogDiffSet $mapperLogDiffSet
     * @param \Mapper_LogDiff $mapperLogDiff
     */
    public function __construct(\Mapper_LogDiffSet $mapperLogDiffSet, \Mapper_LogDiff $mapperLogDiff)
    {
        $this->mapperLogDiffSet = $mapperLogDiffSet;
        $this->mapperLogDiff = $mapperLogDiff;
    }

    /**
     * @param object $prototypeBefore
     * @param object $prototypeAfter
     * @return \Entity_LogDiffSet
     */
    public function create($prototypeBefore, $prototypeAfter)
    {
        $before = (new ObjectToArray())->toArray($prototypeBefore);
        $after = (new ObjectToArray())->toArray($prototypeAfter);
        $changes = (new MapDiffer())->doDiff($before, $after);

        if (count($changes) > 0) {
            $logDiffSet = $this->mapperLogDiffSet->insertEntity([]);
            /**
             * @var string $field
             * @var DiffOp $change
             */
            foreach ($changes as $field => $change) {
                $this->mapperLogDiff->insertEntity([
                    'log_diffset_id' => $logDiffSet->getId(),
                    'field' => $field,
                    'before' => method_exists($change, 'getOldValue') ? $change->getOldValue() : null,
                    'after' => method_exists($change, 'getNewValue') ? $change->getNewValue() : null
                ]);
            }
        }

        return isset($logDiffSet) ? $logDiffSet : null;
    }
}