<?php
namespace Project\Module\SystemModule;

trait DatabaseTrait
{
    /**
     * @return \Mapper_Account
     */
    public function getMapperAccount()
    {
        return $this->getSFM()->getRepository()->get('Mapper_Account');
    }

    /**
     * @return \Mapper_LogAction
     */
    public function getMapperLogAction()
    {
        return $this->getSFM()->getRepository()->get('Mapper_LogAction');
    }

    /**
     * @return \Mapper_LogDiff
     */
    public function getMapperLogDiff()
    {
        return $this->getSFM()->getRepository()->get('Mapper_LogDiff');
    }

    /**
     * @return \Mapper_LogDiffSet
     */
    public function getMapperLogDiffSet()
    {
        return $this->getSFM()->getRepository()->get('Mapper_LogDiffSet');
    }
}