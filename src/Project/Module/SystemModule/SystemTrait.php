<?php
namespace Project\Module\SystemModule;

use Project\Module\SystemModule\Service\LogDiffCreationService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Translation\Translator;

trait SystemTrait
{
    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this['translator'];
    }

    /**
     * @return \Twig_Environment
     */
    public function getTwig()
    {
        return $this["twig"];
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this["request"];
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this["session"];
    }

    /**
     * @return UrlGenerator
     */
    public function getUrlGenerator()
    {
        return $this["url_generator"];
    }

    /**
     * @return SecurityContext
     */
    public function getSecurity()
    {
        return $this["security"];
    }

    /**
     * @return FormFactoryInterface
     */
    public function getFormFactory()
    {
        return $this['form.factory'];
    }

    /**
     * @return EventDispatcher
     */
    public function getDispatcher()
    {
        return $this['dispatcher'];
    }
}