<?php
namespace Project\Module\SystemModule;

use Cron\Resolver\ArrayResolver;
use Silex\Application;
use Silex\ServiceProviderInterface;

class CronSystemProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     */
    public function register(Application $app)
    {
        $app['cron'] = $app->share(function() use ($app) {
            return new ArrayResolver([]);
        });
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {

    }
}