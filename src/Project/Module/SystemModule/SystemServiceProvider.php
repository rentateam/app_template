<?php
namespace Project\Module\SystemModule;

use Astartsky\Menu\MenuItem;
use Project\Module\SystemModule\Service\LogDiffCreationService;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Silex\Translator;
use Symfony\Component\Translation\Loader\YamlFileLoader;

class SystemServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     * @param Application $app An Application instance
     */
    public function register(Application $app)
    {
        /** @var \Project\Application $app */

        $app['translator'] = $app->extend('translator', function ($translator) {
            /** @var $translator Translator */
            $translator->addLoader('yaml', new YamlFileLoader());
            $translator->addResource('yaml', 'locales/en.yml', 'en');
            $translator->addResource('yaml', 'locales/validators_en.yml', 'en', 'validators');

            return $translator;
        });

        $app['left_menu'] = $app->share(function() use ($app) {
            $menuItem = new MenuItem();
            return $menuItem;
        });

        $app['user_menu'] = $app->share(function() use ($app) {
            $menuItem = new MenuItem();

            $logout = new MenuItem($app->getTranslator()->trans('Logout'), $app->getUrlGenerator()->generate('admin_logout'));
            $menuItem->addItem($logout);

            return $menuItem;
        });
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {
    }
}