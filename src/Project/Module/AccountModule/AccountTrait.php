<?php
namespace Project\Module\AccountModule;

use Project\Module\AccountModule\Service\AccountAvatarService;
use Project\Module\AccountModule\Service\AccountChangePasswordService;

trait AccountTrait
{
    /**
     * @return AccountProvider
     */
    public function getAccountProvider()
    {
        return $this['user.provider'];
    }

    /**
     * @return AccountChangePasswordService
     */
    public function getAccountChangePasswordService()
    {
        return $this['service.account_change_password'];
    }

    /**
     * @return AccountAvatarService
     */
    public function getAccountAvatarService()
    {
        return $this['service.account_avatar'];
    }
}