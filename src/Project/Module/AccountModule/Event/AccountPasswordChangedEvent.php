<?php
namespace Project\Module\AccountModule\Event;

use Symfony\Component\EventDispatcher\Event;

class AccountPasswordChangedEvent extends Event
{
    const NAME = 'AccountPasswordChangedEvent';

    protected $account;
    protected $author;

    /**
     * @param \Entity_Account $account
     * @param \Entity_Account $author
     */
    public function __construct(\Entity_Account $account, \Entity_Account $author)
    {
        $this->account = $account;
        $this->author = $author;
    }

    /**
     * @return \Entity_Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return \Entity_Account
     */
    public function getAuthor()
    {
        return $this->author;
    }
}