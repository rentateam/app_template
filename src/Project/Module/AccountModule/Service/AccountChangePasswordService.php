<?php
namespace Project\Module\AccountModule\Service;

use Project\Module\AccountModule\Event\AccountPasswordChangedEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class AccountChangePasswordService
{
    protected $encoderFactory;
    protected $mapperAccount;
    protected $dispatcher;
    protected $accountPrototypeFactory;

    /**
     * @param \Mapper_Account $mapperAccount
     * @param EncoderFactory $encoderFactory
     * @param EventDispatcher $dispatcher
     */
    public function __construct(\Mapper_Account $mapperAccount, EncoderFactory $encoderFactory, EventDispatcher $dispatcher)
    {
        $this->mapperAccount = $mapperAccount;
        $this->encoderFactory = $encoderFactory;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param \Entity_Account $account
     * @param string $password
     * @param \Entity_Account $author
     * @return \Entity_Account
     */
    public function set(\Entity_Account $account, $password, \Entity_Account $author)
    {
        /** @var PasswordEncoderInterface $encoder */
        $encoder = $this->encoderFactory->getEncoder($account);
        $hash = $encoder->encodePassword($password, $account->getSalt());

        $this->mapperAccount->setPassword($account, $hash);

        $this->dispatcher->dispatch(AccountPasswordChangedEvent::NAME, new AccountPasswordChangedEvent($account, $author));

        return $account;
    }
} 