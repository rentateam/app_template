<?php
namespace Project\Module\AccountModule\Service;

use forxer\Gravatar\Gravatar;

class AccountAvatarService
{
    /**
     * @param \Entity_Account $account
     * @return string
     */
    public function getDashboardAvatar(\Entity_Account $account)
    {
        return Gravatar::image($account->getEmail(), 50, 'mm');
    }
}