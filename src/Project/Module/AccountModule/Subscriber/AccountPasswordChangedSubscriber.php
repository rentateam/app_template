<?php
namespace Project\Module\AccountModule\Subscriber;

use Project\Module\AccountModule\Event\AccountPasswordChangedEvent;
use SFM\Database\DatabaseProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Zend\Db\Sql\Sql;

class AccountPasswordChangedSubscriber implements EventSubscriberInterface
{
    protected $db;

    /**
     * @param DatabaseProvider $db
     */
    public function __construct(DatabaseProvider $db)
    {
        $this->db = $db;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            AccountPasswordChangedEvent::NAME => 'onAccountPasswordChange'
        ];
    }

    /**
     * @param AccountPasswordChangedEvent $event
     */
    public function onAccountPasswordChange(AccountPasswordChangedEvent $event)
    {
        $datetime = new \DateTime();
        $sql = new Sql($this->db->getAdapter());
        $insert = $sql
            ->insert('AccountLog')
            ->values([
                'account_id' => $event->getAccount()->getId(),
                'log_action_id' => \Entity_LogAction::ID_ACCOUNT_PASSWORD_CHANGED,
                'entity_id' => $event->getAccount()->getId(),
                'log_diffset_id' => null,
                'datetime' => $datetime->format('Y-m-d H:i:s')
            ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $this->db->insert($statement->getSql(), $statement->getParameterContainer()->getNamedArray());
    }
}