<?php
namespace Project\Module\AccountModule;

class RoleHierarchyBuilder
{
    /** @var \Mapper_AccountRole  */
    protected $mapperAccountRole;

    /**
     * @param \Mapper_AccountRole $mapperAccountRole
     */
    public function __construct(\Mapper_AccountRole $mapperAccountRole)
    {
        $this->mapperAccountRole = $mapperAccountRole;
    }

    /**
     * @return array
     */
    public function getTree()
    {
        $hierarchy = array();
        foreach ($this->getHierarchyMap() as $row) {
            if (false === isset($hierarchy[$row['role_code']])) {
                $hierarchy[$row['role_code']] = array();
            }

            $hierarchy[$row['role_code']][] = $row['privilege_code'];
        }

        return $hierarchy;
    }

    /**
     * @return array
     */
    public function getHierarchyMap()
    {
        try {
            $rows = $this->mapperAccountRole->getHierarchies();
        } catch (\Exception $e) {
            $rows = array();
        }

        return $rows;
    }
}