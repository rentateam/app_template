<?php
namespace Project\Module\AccountModule;

use Project\Module\AccountModule\Controller\AccountController;
use Project\Module\AccountModule\Service\AccountAvatarService;
use Project\Module\AccountModule\Service\AccountChangePasswordService;
use Project\Module\AccountModule\Subscriber\AccountPasswordChangedSubscriber;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AccountServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     */
    public function register(Application $app)
    {
        /** @var $app \Project\Application */

        $app['controller.account'] = $app->share(function() use ($app) {
            return new AccountController($app);
        });

        $app["user.provider"] = $app->share(function () use ($app) {
            return new AccountProvider($app->getSFM()->getRepository()->get('Mapper_Account'));
        });

        $app['roles_hierarchy_builder'] = $app->share(function () use ($app) {
            return new RoleHierarchyBuilder($app->getSFM()->getRepository()->get('Mapper_AccountRole'));
        });

        $app['security.role_hierarchy'] = $app->share(function () use ($app) {
            return $app['roles_hierarchy_builder']->getTree();
        });

        $app['service.account_change_password'] = $app->share(function() use ($app) {
            return new AccountChangePasswordService($app->getSFM()->getRepository()->get('Mapper_Account'), $app['security.encoder_factory'], $app->getDispatcher());
        });

        $app['service.account_avatar'] = $app->share(function() use ($app) {
            return new AccountAvatarService();
        });
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {
        $app->get('/admin/login', 'controller.account:loginAction')
            ->bind('admin_login');

        $app->get('/admin', 'controller.account:indexAction')
            ->bind('admin_index');

        /** Events */

        $app['dispatcher'] = $app->share($app->extend('dispatcher', function($dispatcher) use ($app) {
            /** @var $dispatcher EventDispatcher */
            $dispatcher->addSubscriber(new AccountPasswordChangedSubscriber($app->getSFM()->getDb()));

            return $dispatcher;
        }));
    }
}