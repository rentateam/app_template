<?php
namespace Project\Module\AccountModule\Controller;

use Project\Controller;

class AccountController extends Controller
{
    public function loginAction()
    {
        return $this->app->getTwig()->render('admin/account/login.html.twig', array(
            'error' => $this->app["security.last_error"]->__invoke($this->app->getRequest()),
            'last_username' => $this->app->getSession()->get('_security.last_username')
        ));
    }

    public function indexAction()
    {
        return $this->app->getTwig()->render('admin/index.html.twig');
    }
}