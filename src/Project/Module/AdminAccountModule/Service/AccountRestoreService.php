<?php
namespace Project\Module\AdminAccountModule\Service;

use Project\Module\AdminAccountModule\Event\AccountRestoredEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AccountRestoreService
{
    protected $mapperAccount;
    protected $dispatcher;

    /**
     * @param \Mapper_Account $mapperAccount
     * @param EventDispatcher $dispatcher
     */
    public function __construct(\Mapper_Account $mapperAccount, EventDispatcher $dispatcher)
    {
        $this->mapperAccount = $mapperAccount;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param \Entity_Account $account
     * @param \Entity_Account|null $author
     */
    public function restore(\Entity_Account $account, \Entity_Account $author = null)
    {
        $this->mapperAccount->updateEntity([
            'is_deleted' => 0
        ], $account);

        $this->dispatcher->dispatch(AccountRestoredEvent::NAME, new AccountRestoredEvent($account, $author));
    }
}