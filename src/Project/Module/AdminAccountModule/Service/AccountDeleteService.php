<?php
namespace Project\Module\AdminAccountModule\Service;

use Project\Module\AdminAccountModule\Event\AccountDeletedEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AccountDeleteService
{
    protected $mapperAccount;
    protected $dispatcher;

    /**
     * @param \Mapper_Account $mapperAccount
     * @param EventDispatcher $dispatcher
     */
    public function __construct(\Mapper_Account $mapperAccount, EventDispatcher $dispatcher)
    {
        $this->mapperAccount = $mapperAccount;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param \Entity_Account $account
     * @param \Entity_Account $author
     */
    public function delete(\Entity_Account $account, \Entity_Account $author)
    {
        $this->mapperAccount->updateEntity([
            'is_deleted'=> 1
        ], $account);

        $this->dispatcher->dispatch(AccountDeletedEvent::NAME, new AccountDeletedEvent($account, $author));
    }
}