<?php
namespace Project\Module\AdminAccountModule\Service;

use Project\Module\AccountModule\Service\AccountChangePasswordService;
use Project\Module\AdminAccountModule\Event\AccountCreatedEvent;
use Prototype\AccountPrototype;
use PrototypeFactory\AccountPrototypeFactory;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AccountCreateService
{
    protected $mapperAccount;
    protected $changePasswordService;
    protected $dispatcher;
    protected $accountPrototypeFactory;

    /**
     * @param \Mapper_Account $mapperAccount
     * @param AccountChangePasswordService $changePasswordService
     * @param EventDispatcher $dispatcher
     * @param AccountPrototypeFactory $accountPrototypeFactory
     */
    public function __construct(\Mapper_Account $mapperAccount, AccountChangePasswordService $changePasswordService, EventDispatcher $dispatcher, AccountPrototypeFactory $accountPrototypeFactory)
    {
        $this->mapperAccount = $mapperAccount;
        $this->changePasswordService = $changePasswordService;
        $this->dispatcher = $dispatcher;
        $this->accountPrototypeFactory = $accountPrototypeFactory;
    }

    /**
     * @param AccountPrototype $prototype
     * @param \Entity_Account $author
     */
    public function create(AccountPrototype $prototype, \Entity_Account $author)
    {
        $data = [
            'name' => $prototype->getName(),
            'email' => $prototype->getEmail(),
            'account_role_id' => 1,
            'salt' => md5(microtime(true)),
            'password' => '',
            'is_deleted' => 0
        ];

        /** @var \Entity_Account $account */
        $account = $this->mapperAccount->insertEntity($data);

        $prototypeAfter = $this->accountPrototypeFactory->createPrototype($account);
        $this->dispatcher->dispatch(AccountCreatedEvent::NAME, new AccountCreatedEvent($account, $prototypeAfter, $author));

        $this->changePasswordService->set($account, $prototype->getPassword(), $author);
    }
}