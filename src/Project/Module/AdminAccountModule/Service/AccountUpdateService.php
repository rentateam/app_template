<?php
namespace Project\Module\AdminAccountModule\Service;

use Project\Module\AdminAccountModule\Event\AccountUpdatedEvent;
use Prototype\AccountPrototype;
use PrototypeFactory\AccountPrototypeFactory;
use SFM\Manager;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AccountUpdateService
{
    protected $dispatcher;
    protected $mapperAccount;
    protected $accountPrototypeFactory;

    /**
     * @param \Mapper_Account $mapperAccount
     * @param EventDispatcher $dispatcher
     * @param AccountPrototypeFactory $accountPrototypeFactory
     */
    public function __construct(\Mapper_Account $mapperAccount, EventDispatcher $dispatcher, AccountPrototypeFactory $accountPrototypeFactory)
    {
        $this->dispatcher = $dispatcher;
        $this->mapperAccount = $mapperAccount;
        $this->accountPrototypeFactory = $accountPrototypeFactory;
    }

    /**
     * @param \Entity_Account $account
     * @param AccountPrototype $prototype
     * @param \Entity_Account $author
     */
    public function update(\Entity_Account $account, AccountPrototype $prototype, \Entity_Account $author)
    {
        $data = [
            'name' => $prototype->getName(),
            'email' => $prototype->getEmail(),
            'account_role_id' => $prototype->getAccountRoleId()
        ];

        $prototypeBefore = $this->accountPrototypeFactory->createPrototype($account);
        $this->mapperAccount->updateEntity($data, $account);
        $prototypeAfter = $this->accountPrototypeFactory->createPrototype($account);

        $this->dispatcher->dispatch(AccountUpdatedEvent::NAME, new AccountUpdatedEvent($account, $prototypeBefore, $prototypeAfter, $author));
    }
}