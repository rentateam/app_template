<?php
namespace Project\Module\AdminAccountModule\Log;

use Criteria\AbstractCriteria;
use Project\Module\AdminLogModule\LogInternal\LogCriteria;
use QueryBuilder\AbstractQueryBuilder;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;

class AccountLogBuilder extends AbstractQueryBuilder
{
    /**
     * @param AbstractCriteria $criteria
     * @return Select
     */
    public function createSelect(AbstractCriteria $criteria)
    {
        /** @var LogCriteria $criteria */

        $columns = $criteria->isOnlyCountRows() ? $columns = ['count' => new Expression("COUNT(id)")] : ['*', 'entity_type' => new Expression("'Account'")];
        $select = $this->sql->select()
            ->from(['al' => 'AccountLog'])
            ->columns($columns);

        return $select;
    }

    /**
     * @param AbstractCriteria $criteria
     * @return bool
     */
    public function isValidCriteria(AbstractCriteria $criteria)
    {
        return $criteria instanceof LogCriteria;
    }
}