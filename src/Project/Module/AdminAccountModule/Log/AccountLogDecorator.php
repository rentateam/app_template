<?php
namespace Project\Module\AdminAccountModule\Log;

use Project\Module\AdminLogModule\LogInternal\LogDecorator\LogDecoratorInterface;
use Project\Module\AdminLogModule\LogInternal\LogEntry\AccountLogEntry;

class AccountLogDecorator implements LogDecoratorInterface
{
    protected $mapperAccount;
    protected $mapperLogAction;
    protected $mapperLogDiffSet;

    /**
     * @param \Mapper_Account $mapperAccount
     * @param \Mapper_LogAction $mapperLogAction
     * @param \Mapper_LogDiffSet $mapperLogDiffSet
     */
    public function __construct(\Mapper_Account $mapperAccount, \Mapper_LogAction $mapperLogAction, \Mapper_LogDiffSet $mapperLogDiffSet)
    {
        $this->mapperAccount = $mapperAccount;
        $this->mapperLogAction = $mapperLogAction;
        $this->mapperLogDiffSet = $mapperLogDiffSet;
    }

    /**
     * @param array $rawLog
     * @return bool
     */
    public function isValid($rawLog)
    {
        return isset($rawLog['entity_type']) && $rawLog['entity_type'] == 'Account';
    }

    /**
     * @param array $rawLog
     * @return AccountLogEntry
     */
    public function decorateLog($rawLog)
    {
        /** @var \Entity_Account $author */
        $author = (isset($rawLog['account_id']) && intval($rawLog['account_id']) > 0) ? $this->mapperAccount->getEntityById($rawLog['account_id']) : null;

        /** @var \Entity_LogAction $action */
        $action = $this->mapperLogAction->getEntityById($rawLog['log_action_id']);

        /** @var \Entity_Account $entity */
        $entity = $this->mapperAccount->getEntityById($rawLog['entity_id']);

        /** @var \Entity_LogDiffSet $diffset */
        $diffset = (isset($rawLog['log_diffset_id']) && intval($rawLog['log_diffset_id']) > 0) ? $this->mapperLogDiffSet->getEntityById($rawLog['log_diffset_id']) : null;
        $datetime = new \DateTime($rawLog['datetime']);

        return new AccountLogEntry($rawLog['id'], $entity, $action, $datetime, 'admin/log/entry/account.html.twig', $diffset, $author);
    }
}