<?php
namespace Project\Module\AdminAccountModule\Subscriber;

use Project\Module\AdminAccountModule\Event\AccountRestoredEvent;
use SFM\Database\DatabaseProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Zend\Db\Sql\Sql;

class AccountRestoredSubscriber implements EventSubscriberInterface
{
    protected $db;

    /**
     * @param DatabaseProvider $db
     */
    public function __construct(DatabaseProvider $db)
    {
        $this->db = $db;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            AccountRestoredEvent::NAME => 'onAccountRestore'
        ];
    }

    /**
     * @param AccountRestoredEvent $event
     */
    public function onAccountRestore(AccountRestoredEvent $event)
    {
        $datetime = new \DateTime();
        $sql = new Sql($this->db->getAdapter());
        $insert = $sql
            ->insert('AccountLog')
            ->values([
                'account_id' => $event->getAuthor()->getId(),
                'log_action_id' => \Entity_LogAction::ID_ACCOUNT_RESTORED,
                'entity_id' => $event->getAccount()->getId(),
                'datetime' => $datetime->format('Y-m-d H:i:s')
            ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $this->db->insert($statement->getSql(), $statement->getParameterContainer()->getNamedArray());
    }
}