<?php
namespace Project\Module\AdminAccountModule\Subscriber;

use Project\Module\AdminAccountModule\Event\AccountCreatedEvent;
use Project\Module\SystemModule\Service\LogDiffCreationService;
use Prototype\AccountPrototype;
use SFM\Database\DatabaseProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Zend\Db\Sql\Sql;

class AccountCreatedSubscriber implements EventSubscriberInterface
{
    /**
     * @param DatabaseProvider $db
     * @param LogDiffCreationService $logDiffCreationService
     */
    public function __construct(DatabaseProvider $db, LogDiffCreationService $logDiffCreationService)
    {
        $this->db = $db;
        $this->logDiffCreationService = $logDiffCreationService;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            AccountCreatedEvent::NAME => 'onAccountCreate'
        ];
    }

    /**
     * @param AccountCreatedEvent $event
     */
    public function onAccountCreate(AccountCreatedEvent $event)
    {
        $logDiffSet = $this->logDiffCreationService->create(new AccountPrototype(), $event->getAfterPrototype());
        $datetime = new \DateTime();

        if ($logDiffSet !== null) {
            $sql = new Sql($this->db->getAdapter());
            $insert = $sql
                ->insert('AccountLog')
                ->values([
                    'account_id' => $event->getAuthor()->getId(),
                    'log_action_id' => \Entity_LogAction::ID_ACCOUNT_CREATED,
                    'entity_id' => $event->getAccount()->getId(),
                    'log_diffset_id' => $logDiffSet->getId(),
                    'datetime' => $datetime->format('Y-m-d H:i:s')
                ]);

            $statement = $sql->prepareStatementForSqlObject($insert);
            $this->db->insert($statement->getSql(), $statement->getParameterContainer()->getNamedArray());
        }
    }
}