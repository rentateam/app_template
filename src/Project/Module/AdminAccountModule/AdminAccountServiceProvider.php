<?php
namespace Project\Module\AdminAccountModule;

use Astartsky\Menu\MenuItem;
use Constraint\AccountEmailUniqueValidator;
use Converter\AccountConverter;
use Project\Module\AdminAccountModule\Controller\AccountController;
use Project\Module\AdminAccountModule\Log\AccountLogBuilder;
use Project\Module\AdminAccountModule\Log\AccountLogDecorator;
use Project\Module\AdminAccountModule\Service\AccountCreateService;
use Project\Module\AdminAccountModule\Service\AccountDeleteService;
use Project\Module\AdminAccountModule\Service\AccountRestoreService;
use Project\Module\AdminAccountModule\Service\AccountUpdateService;
use Project\Module\AdminAccountModule\Subscriber\AccountCreatedSubscriber;
use Project\Module\AdminAccountModule\Subscriber\AccountDeletedSubscriber;
use Project\Module\AdminAccountModule\Subscriber\AccountRestoredSubscriber;
use Project\Module\AdminAccountModule\Subscriber\AccountUpdatedSubscriber;
use Project\Module\AdminLogModule\LogInternal\LogBuilder;
use Project\Module\AdminLogModule\LogInternal\LogDecorator;
use PrototypeFactory\AccountPrototypeFactory;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class AdminAccountServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     */
    public function register(Application $app)
    {
        /** @var \Project\Application $app */

        $app['controller.admin.account'] = $app->share(function() use ($app) {
            return new AccountController($app);
        });

        $app['left_menu'] = $app->share($app->extend('left_menu', function(MenuItem $menu) use ($app) {
            if ($app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
                $item = new MenuItem(
                    $app->getTranslator()->trans('Accounts'),
                    $app->getUrlGenerator()->generate('admin_accounts_list'),
                    ['admin_accounts_list'],
                    ['icon' => 'fa-users']
                );
                $menu->addItem($item);
            }

            return $menu;
        }));

        $app['converter.account'] = $app->share(function() use ($app) {
            return new AccountConverter($app->getMapperAccount());
        });

        $app['service.account_update'] = $app->share(function() use ($app) {
            return new AccountUpdateService($app->getMapperAccount(), $app->getDispatcher(), new AccountPrototypeFactory());
        });

        $app['service.account_create'] = $app->share(function() use ($app) {
            return new AccountCreateService($app->getMapperAccount(), $app->getAccountChangePasswordService(), $app->getDispatcher(), new AccountPrototypeFactory());
        });

        $app['service.account_delete'] = $app->share(function() use ($app) {
            return new AccountDeleteService($app->getMapperAccount(), $app->getDispatcher());
        });

        $app['service.account_restore'] = $app->share(function() use ($app) {
            return new AccountRestoreService($app->getMapperAccount(), $app->getDispatcher());
        });

        $app['validator.account_email_unique'] = $app->share(function() use ($app) {
            return new AccountEmailUniqueValidator($app->getMapperAccount());
        });

        $app['log_builder'] = $app->share($app->extend('log_builder', function($logBuilder) use ($app) {
            /** @var LogBuilder $logBuilder */
            $logBuilder->registerBuilder(new AccountLogBuilder($app->getSFM()->getDb()->getAdapter()));

            return $logBuilder;
        }));

        $app['log_decorator'] = $app->share($app->extend('log_decorator', function($logDecorator) use ($app) {
            /** @var LogDecorator $logDecorator */
            $logDecorator->registerLogDecorator(new AccountLogDecorator($app->getMapperAccount(), $app->getMapperLogAction(), $app->getMapperLogDiffSet()));

            return $logDecorator;
        }));
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {
        /** @var \Project\Application $app */

        $app->get('/admin/accounts', 'controller.admin.account:listAction')
            ->bind('admin_accounts_list');

        $app->get('/admin/accounts/{account}/edit', 'controller.admin.account:editAction')
            ->convert('account', 'converter.account:convert')
            ->bind('admin_accounts_edit');

        $app->post('/admin/accounts/{account}/edit', 'controller.admin.account:updateAction')
            ->convert('account', 'converter.account:convert');

        $app->get('/admin/accounts/create', 'controller.admin.account:createAction')
            ->bind('admin_accounts_create');

        $app->post('/admin/accounts/create', 'controller.admin.account:saveAction');

        $app->get('/admin/accounts/{account}/password', 'controller.admin.account:passwordAction')
            ->convert('account', 'converter.account:convert')
            ->bind('admin_accounts_password');

        $app->post('/admin/accounts/{account}/password', 'controller.admin.account:changePasswordAction')
            ->convert('account', 'converter.account:convert');

        $app->delete('/admin/accounts/{account}/delete', 'controller.admin.account:deleteAction')
            ->convert('account', 'converter.account:convert')
            ->bind('admin_accounts_delete');

        $app->post('/admin/accounts/{account}/restore', 'controller.admin.account:restoreAction')
            ->convert('account', 'converter.account:convert')
            ->bind('admin_accounts_restore');

        /** Events */

        $app['dispatcher'] = $app->share($app->extend('dispatcher', function($dispatcher) use ($app) {
            /** @var $dispatcher EventDispatcher */
            $dispatcher->addSubscriber(new AccountUpdatedSubscriber($app->getSFM()->getDb(), $app->getLogDiffCreationService()));
            $dispatcher->addSubscriber(new AccountCreatedSubscriber($app->getSFM()->getDb(), $app->getLogDiffCreationService()));
            $dispatcher->addSubscriber(new AccountDeletedSubscriber($app->getSFM()->getDb()));
            $dispatcher->addSubscriber(new AccountRestoredSubscriber($app->getSFM()->getDb()));

            return $dispatcher;
        }));
    }
}