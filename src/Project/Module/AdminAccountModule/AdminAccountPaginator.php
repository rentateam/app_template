<?php
namespace Project\Module\AdminAccountModule;

use Astartsky\Paginator\Page;
use Astartsky\Paginator\Paginator;
use Symfony\Component\Routing\Generator\UrlGenerator;

class AdminAccountPaginator extends Paginator
{
    protected $urlGenerator;

    /**
     * @param UrlGenerator $urlGenerator
     * @param int $total
     * @param int $perPage
     * @param int $current
     * @param int $limit
     * @throws \Astartsky\Paginator\Exception
     */
    public function __construct(UrlGenerator $urlGenerator, $total, $perPage, $current, $limit = 10)
    {
        $this->urlGenerator = $urlGenerator;
        parent::__construct($total, $perPage, $current, $limit);
    }

    /**
     * @param int $i
     * @return Page[]
     */
    protected function createPage($i)
    {
        return new Page($i + 1, $i + 1 == $this->getCurrentIndex(), $this->urlGenerator->generate('admin_accounts_list', ['page' => $i + 1]));
    }
}