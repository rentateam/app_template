<?php
namespace Project\Module\AdminAccountModule\Event;

use Symfony\Component\EventDispatcher\Event;

class AccountRestoredEvent extends Event
{
    const NAME = 'AccountRestoredEvent';

    protected $account;
    protected $author;

    /**
     * @param \Entity_Account $account
     * @param \Entity_Account|null $author
     */
    public function __construct(\Entity_Account $account, \Entity_Account $author = null)
    {
        $this->account = $account;
        $this->author = $author;
    }

    /**
     * @return \Entity_Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return \Entity_Account|null
     */
    public function getAuthor()
    {
        return $this->author;
    }
}