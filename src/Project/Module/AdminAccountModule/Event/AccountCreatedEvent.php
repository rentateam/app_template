<?php
namespace Project\Module\AdminAccountModule\Event;

use Prototype\AccountPrototype;
use Symfony\Component\EventDispatcher\Event;

class AccountCreatedEvent extends Event
{
    const NAME = 'AccountCreatedEvent';

    protected $account;
    protected $author;
    protected $afterPrototype;

    /**
     * @param \Entity_Account $account
     * @param AccountPrototype $afterPrototype
     * @param \Entity_Account|null $author
     */
    public function __construct(\Entity_Account $account, AccountPrototype $afterPrototype, \Entity_Account $author = null)
    {
        $this->account = $account;
        $this->author = $author;
        $this->afterPrototype = $afterPrototype;
    }

    /**
     * @return \Entity_Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return \Entity_Account|null
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return AccountPrototype
     */
    public function getAfterPrototype()
    {
        return $this->afterPrototype;
    }
}