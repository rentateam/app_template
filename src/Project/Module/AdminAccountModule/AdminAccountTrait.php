<?php
namespace Project\Module\AdminAccountModule;

use Project\Module\AdminAccountModule\Service\AccountCreateService;
use Project\Module\AdminAccountModule\Service\AccountDeleteService;
use Project\Module\AdminAccountModule\Service\AccountRestoreService;
use Project\Module\AdminAccountModule\Service\AccountUpdateService;

trait AdminAccountTrait
{
    /**
     * @return AccountUpdateService
     */
    public function getAccountUpdateService()
    {
        return $this['service.account_update'];
    }

    /**
     * @return AccountCreateService
     */
    public function getAccountCreateService()
    {
        return $this['service.account_create'];
    }

    /**
     * @return AccountDeleteService
     */
    public function getAccountDeleteService()
    {
        return $this['service.account_delete'];
    }

    /**
     * @return AccountRestoreService
     */
    public function getAccountRestoreService()
    {
        return $this['service.account_restore'];
    }
}