<?php
namespace Project\Module\AdminAccountModule\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountCreateFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'account_create';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');

        $builder->add('email', 'text', [
            'label' => 'Email'
        ]);

        $builder->add('name', 'text', [
            'label' => 'Name'
        ]);

        $builder->add('password', 'repeated', [
            'type' => 'password',
            'invalid_message' => 'Passwords are not equal',
            'first_options' => ['label' => 'Password'],
            'second_options' => ['label' => 'Confirm password']
        ]);

        $builder->add('submit', 'submit', [
            'label' => 'Save'
        ]);

        $builder->add('account_role_id', 'hidden');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Prototype\AccountPrototype',
            'validation_groups' => ['create']
        ]);
    }
}