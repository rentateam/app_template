<?php
namespace Project\Module\AdminAccountModule\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AccountPasswordFormType extends AbstractType
{
    /**
     * @return string The name of this type
     */
    public function getName()
    {
        return 'change_password';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'hidden');

        $builder->add('password', 'repeated', [
            'type' => 'password',
            'invalid_message' => 'Passwords are not equal',
            'first_options' => ['label' => 'Password'],
            'second_options' => ['label' => 'Confirm password']
        ]);

        $builder->add('submit', 'submit', [
            'label' => 'Change'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Prototype\AccountPrototype',
            'validation_groups' => ['change_password']
        ]);
    }
}