<?php
namespace Project\Module\AdminAccountModule\Controller;

use Criteria\AccountCriteria;
use Project\Controller;
use Project\Module\AdminAccountModule\AdminAccountPaginator;
use Project\Module\AdminAccountModule\FormType\AccountCreateFormType;
use Project\Module\AdminAccountModule\FormType\AccountEditFormType;
use Project\Module\AdminAccountModule\FormType\AccountPasswordFormType;
use PrototypeFactory\AccountPrototypeFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccountController extends Controller
{
    public function listAction()
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $page = $this->app->getRequest()->get('page', 1);
        $perPage = 10;

        $criteria = new AccountCriteria();
        $accounts = $this->app->getMapperAccount()->getList($criteria);
        $accounts->loadEntitiesForCurrentPage($page, $perPage);

        $paginator = new AdminAccountPaginator($this->app->getUrlGenerator(), $accounts->totalCount(), $perPage, $page);

        return $this->app->getTwig()->render('admin/account/list.html.twig', [
            'accounts' => $accounts,
            'pages' => $paginator
        ]);
    }

    public function editAction(\Entity_Account $account)
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->app->getFormFactory()->create(new AccountEditFormType(), (new AccountPrototypeFactory())->createPrototype($account));

        return $this->app->getTwig()->render('admin/account/edit.html.twig', [
            'form_view' => $form->createView()
        ]);
    }

    public function updateAction(\Entity_Account $account)
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->app->getFormFactory()->create(new AccountEditFormType());
        $form->handleRequest($this->app->getRequest());

        if ($form->isValid()) {
            $this->app->getAccountUpdateService()->update($account, $form->getData(), $this->app->getSecurity()->getToken()->getUser());
            $this->app->getSession()->set('_flash_success', $this->app->getTranslator()->trans('Saved'));
            return new RedirectResponse($this->app->getUrlGenerator()->generate('admin_accounts_list'));
        }

        return $this->app->getTwig()->render('admin/account/edit.html.twig', [
            'form_view' => $form->createView()
        ]);
    }

    public function createAction()
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->app->getFormFactory()->create(new AccountCreateFormType());

        return $this->app->getTwig()->render('admin/account/create.html.twig', [
            'form_view' => $form->createView()
        ]);
    }

    public function saveAction()
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->app->getFormFactory()->create(new AccountCreateFormType());
        $form->handleRequest($this->app->getRequest());

        if ($form->isValid()) {
            $this->app->getAccountCreateService()->create($form->getData(), $this->app->getSecurity()->getToken()->getUser());
            $this->app->getSession()->set('_flash_success', $this->app->getTranslator()->trans('Saved'));
            return new RedirectResponse($this->app->getUrlGenerator()->generate('admin_accounts_list'));
        }

        return $this->app->getTwig()->render('admin/account/password.html.twig', [
            'form_view' => $form->createView()
        ]);
    }

    public function passwordAction(\Entity_Account $account)
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->app->getFormFactory()->create(new AccountPasswordFormType());

        return $this->app->getTwig()->render('admin/account/password.html.twig', [
            'form_view' => $form->createView()
        ]);
    }

    public function changePasswordAction(\Entity_Account $account)
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->app->getFormFactory()->create(new AccountPasswordFormType());
        $form->handleRequest($this->app->getRequest());

        if ($form->isValid()) {
            $this->app->getAccountChangePasswordService()->set($account, $form->getData()->getPassword(), $this->app->getSecurity()->getToken()->getUser());
            $this->app->getSession()->set('_flash_success', $this->app->getTranslator()->trans('Saved'));
            return new RedirectResponse($this->app->getUrlGenerator()->generate('admin_accounts_list'));
        }

        return $this->app->getTwig()->render('admin/account/password.html.twig', [
            'form_view' => $form->createView()
        ]);
    }

    public function deleteAction(\Entity_Account $account)
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $this->app->getAccountDeleteService()->delete($account, $this->app->getSecurity()->getToken()->getUser());

        return new JsonResponse(['status' => 'ok']);
    }

    public function restoreAction(\Entity_Account $account)
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_EDIT_ACCOUNTS')) {
            throw new AccessDeniedHttpException();
        }

        $this->app->getAccountRestoreService()->restore($account, $this->app->getSecurity()->getToken()->getUser());

        return new JsonResponse(['status' => 'ok']);
    }
}