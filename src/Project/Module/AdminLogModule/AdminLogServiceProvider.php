<?php
namespace Project\Module\AdminLogModule;

use Astartsky\Menu\MenuItem;
use Project\Module\AdminAccountModule\Log\AccountLogBuilder;
use Project\Module\AdminLogModule\Controller\LogController;
use Project\Module\AdminLogModule\LogInternal\LogBuilder;
use Project\Module\AdminLogModule\LogInternal\LogDecorator;
use Project\Module\SystemModule\Service\LogDiffCreationService;
use Silex\Application;
use Silex\ServiceProviderInterface;

class AdminLogServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     */
    public function register(Application $app)
    {
        /** @var \Project\Application $app */

        $app['controller.admin.log'] = $app->share(function() use ($app) {
            return new LogController($app);
        });

        $app['left_menu'] = $app->share($app->extend('left_menu', function(MenuItem $menu) use ($app) {
            if ($app->getSecurity()->isGranted('ROLE_VIEW_LOGS')) {
                $item = new MenuItem(
                    $app->getTranslator()->trans('Log'),
                    $app->getUrlGenerator()->generate('admin_log_list'),
                    ['admin_log_list'],
                    ['icon' => 'fa-history']
                );
                $menu->addItem($item);
            }

            return $menu;
        }));

        $app['log_builder'] = $app->share(function() use ($app) {
            return new LogBuilder($app->getSFM()->getDb()->getAdapter());
        });

        $app['log_decorator'] = $app->share(function() use ($app) {
            return new LogDecorator();
        });

        $app['service.create_log_diffset'] = $app->share(function() use ($app) {
            return new LogDiffCreationService($app->getMapperLogDiffSet(), $app->getMapperLogDiff());
        });
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {
        /** @var \Project\Application $app */

        $app->get('/admin/log', 'controller.admin.log:listAction')
            ->bind('admin_log_list');
    }
}