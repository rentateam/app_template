<?php
namespace Project\Module\AdminLogModule\Controller;

use Project\Controller;
use Project\Module\AdminLogModule\LogInternal\LogCriteria;
use Project\Module\AdminLogModule\LogInternal\LogDecorator;
use Project\Module\AdminLogModule\LogInternal\LogSearch;
use Project\Module\AdminLogModule\LogPaginator;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LogController extends Controller
{
    protected $allowedLimit = [
        100, 200, 500
    ];

    public function listAction()
    {
        if (false === $this->app->getSecurity()->isGranted('ROLE_VIEW_LOGS')) {
            throw new AccessDeniedHttpException();
        }

        $criteria = new LogCriteria();
        $criteria->orderById();

        $page = $this->app->getRequest()->get('page', 1);
        $limit = $this->app->getRequest()->get('limit', 100);
        if (!in_array($limit, $this->allowedLimit)) {
            $limit = 100;
        }

        $logSearch = new LogSearch($this->app->getLogBuilder(), $this->app->getLogDecorator(), $this->app->getSFM()->getDb());
        $count = $logSearch->getTotalCount($criteria);
        $logs = $logSearch->getLogIterator($criteria, $page, $limit);

        $paginator = new LogPaginator($this->app->getUrlGenerator(), $count, $limit, $page);

        return $this->app->getTwig()->render('admin/log/list.html.twig', [
            'logs' => $logs,
            'pages' => $paginator
        ]);
    }
}