<?php
namespace Project\Module\AdminLogModule;

use Astartsky\Paginator\Page;
use Astartsky\Paginator\Paginator;
use Symfony\Component\Routing\Generator\UrlGenerator;

class LogPaginator extends Paginator
{
    protected $urlGenerator;

    public function __construct(UrlGenerator $urlGenerator, $total, $perPage, $current, $limit = 10)
    {
        $this->urlGenerator = $urlGenerator;
        parent::__construct($total, $perPage, $current, $limit);
    }

    /**
     * @param int $i
     * @return Page[]
     */
    protected function createPage($i)
    {
        return new Page($i + 1, $this->getCurrentIndex() == $i + 1, $this->urlGenerator->generate('admin_log_list', ['page' => $i + 1]));
    }
}