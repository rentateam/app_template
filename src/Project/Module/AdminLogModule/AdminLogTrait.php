<?php
namespace Project\Module\AdminLogModule;

use Project\Module\AdminLogModule\LogInternal\LogDecorator;
use Project\Module\AdminLogModule\LogInternal\LogBuilder;
use Project\Module\SystemModule\Service\LogDiffCreationService;

trait AdminLogTrait
{
    /**
     * @return LogBuilder
     */
    public function getLogBuilder()
    {
        return $this['log_builder'];
    }

    /**
     * @return LogDecorator
     */
    public function getLogDecorator()
    {
        return $this['log_decorator'];
    }

    /**
     * @return LogDiffCreationService
     */
    public function getLogDiffCreationService()
    {
        return $this['service.create_log_diffset'];
    }
}