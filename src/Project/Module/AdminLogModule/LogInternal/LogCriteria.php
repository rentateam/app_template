<?php
namespace Project\Module\AdminLogModule\LogInternal;

use Criteria\AbstractCriteria;

class LogCriteria extends AbstractCriteria
{
    protected $onlyCountRows = false;
    protected $limit;
    protected $page;
    protected $order;

    public function orderById()
    {
        $this->order = [
            'id' => 'DESC'
        ];
    }

    /**
     * @return array
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return bool
     */
    public function isOnlyCountRows()
    {
        return $this->onlyCountRows;
    }

    /**
     * @param bool $state
     */
    public function setOnlyCountRows($state)
    {
        $this->onlyCountRows = $state;
    }
}