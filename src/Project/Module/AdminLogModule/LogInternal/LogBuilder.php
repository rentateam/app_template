<?php
namespace Project\Module\AdminLogModule\LogInternal;

use QueryBuilder\AbstractQueryBuilder;
use Criteria\AbstractCriteria;
use Zend\Db\Adapter\Driver\StatementInterface;
use Zend\Db\Sql\Select;

class LogBuilder extends AbstractQueryBuilder
{
    protected $logBuilders = [];

    /**
     * @param AbstractQueryBuilder $builder
     */
    public function registerBuilder(AbstractQueryBuilder $builder)
    {
        $this->logBuilders[get_class($builder)] = $builder;
    }

    /**
     * @param AbstractCriteria $criteria
     * @return Select
     * @throws \Exception
     */
    public function createSelect(AbstractCriteria $criteria)
    {
        throw new \Exception();
    }

    public function getStatement(AbstractCriteria $criteria)
    {
        /** @var LogCriteria $criteria */

        $sql = [];
        $params = [];

        /** @var AbstractQueryBuilder $builder */
        foreach ($this->logBuilders as $builder) {
            $statement = $builder->getStatement($criteria);
            $sql[] = $statement->getSql();
            $params = array_merge($params, $statement->getParameterContainer()->getNamedArray());
        }

        $innerSql = implode("\nUNION ALL\n", $sql);
        if ($criteria->isOnlyCountRows()) {
            $sql = "SELECT SUM(`count`) AS `count` FROM (\n{$innerSql}\n) AS `t`";
        } else {
            $sql = "SELECT * FROM (\n{$innerSql}\n) AS `t`";
        }

        if (!$criteria->isOnlyCountRows() && $orderItems = $criteria->getOrder()) {
            $orders = [];
            foreach ($orderItems as $field => $direction) {
                $orders[] = "`t`.`{$field}` {$direction}";
            }
            $sql .= "\nORDER BY " . implode(", ", $orders);
        }

        if ($criteria->getLimit() != null) {
            $offset = ($criteria->getPage() - 1) * $criteria->getLimit();
            $sql .= "\nLIMIT {$offset}, {$criteria->getLimit()}";
        }

        /** @var StatementInterface $statement */
        $statement = $this->sql->getAdapter()->createStatement($sql);

        return $statement;
    }

    /**
     * @param AbstractCriteria $criteria
     * @return bool
     */
    public function isValidCriteria(AbstractCriteria $criteria)
    {
        return $criteria instanceof LogCriteria;
    }
}