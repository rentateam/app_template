<?php
namespace Project\Module\AdminLogModule\LogInternal;

use SFM\Database\DatabaseProvider;

class LogSearch
{
    protected $builder;
    protected $decorator;
    protected $db;

    /**
     * @param LogBuilder $builder
     * @param LogDecorator $decorator
     * @param DatabaseProvider $db
     */
    public function __construct(LogBuilder $builder, LogDecorator $decorator, DatabaseProvider $db)
    {
        $this->builder = $builder;
        $this->decorator = $decorator;
        $this->db = $db;
    }

    /**
     * @param LogCriteria $criteria
     * @return int
     * @throws \SFM\BaseException
     */
    public function getTotalCount(LogCriteria $criteria)
    {
        $countCriteria = clone $criteria;
        $countCriteria->setOnlyCountRows(true);

        $countStatement = $this->builder->getStatement($countCriteria);
        $countResultSet = $this->db->query($countStatement->getSql(), $countStatement->getParameterContainer()->getNamedArray());

        $row = $countResultSet->current();

        return isset($row['count']) ? $row['count'] : 0;
    }

    /**
     * @param LogCriteria $criteria
     * @param int $page
     * @param int $perPage
     * @return LogIterator
     * @throws \SFM\BaseException
     */
    public function getLogIterator(LogCriteria $criteria, $page, $perPage = 5)
    {
        $pagedCriteria = clone $criteria;
        $pagedCriteria->setLimit($perPage);
        $pagedCriteria->setPage($page);

        $statement = $this->builder->getStatement($pagedCriteria);
        $resultSet = $this->db->query($statement->getSql(), $statement->getParameterContainer()->getNamedArray());

        return new LogIterator($resultSet, $this->decorator);
    }
}