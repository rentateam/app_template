<?php
namespace Project\Module\AdminLogModule\LogInternal;

use Project\Module\AdminLogModule\LogInternal\LogDecorator\LogDecoratorInterface;
use Zend\Db\ResultSet\ResultSet;

class LogIterator implements \Iterator
{
    protected $resultSet;
    protected $decorator;

    /**
     * @param ResultSet $resultSet
     */
    public function __construct(ResultSet $resultSet, LogDecorator $decorator)
    {
        $this->resultSet = $resultSet;
        $this->decorator = $decorator;
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->decorator->apply($this->resultSet->current());
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->resultSet->next();
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->resultSet->key();
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return $this->resultSet->valid();
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->resultSet->rewind();
    }
}