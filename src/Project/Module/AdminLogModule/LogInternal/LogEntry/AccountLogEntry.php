<?php
namespace Project\Module\AdminLogModule\LogInternal\LogEntry;

class AccountLogEntry
{
    protected $account;
    protected $action;
    protected $datetime;
    protected $template;
    protected $diffset;
    protected $author;
    protected $id;

    /**
     * @param int $id
     * @param \Entity_Account $account
     * @param \Entity_LogAction $action
     * @param \DateTime $datetime
     * @param string $template
     * @param \Entity_LogDiffSet|null $diffset
     * @param \Entity_Account|null $author
     */
    public function __construct($id, \Entity_Account $account, \Entity_LogAction $action, \DateTime $datetime, $template, \Entity_LogDiffSet $diffset = null, \Entity_Account $author = null)
    {
        $this->id = $id;
        $this->account = $account;
        $this->action = $action;
        $this->datetime = $datetime;
        $this->template = $template;
        $this->diffset = $diffset;
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Entity_Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @return \Entity_LogAction
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return \Entity_LogDiffSet
     */
    public function getDiffset()
    {
        return $this->diffset;
    }

    /**
     * @return \Entity_Account
     */
    public function getAuthor()
    {
        return $this->author;
    }
}