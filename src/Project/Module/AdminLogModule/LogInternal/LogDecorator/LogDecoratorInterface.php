<?php
namespace Project\Module\AdminLogModule\LogInternal\LogDecorator;

use Project\Module\AdminLogModule\LogInternal\LogEntry\AccountLogEntry;

interface LogDecoratorInterface
{
    /**
     * @param array $rawLog
     * @return bool
     */
    public function isValid($rawLog);

    /**
     * @param array $rawLog
     * @return AccountLogEntry
     */
    public function decorateLog($rawLog);
}