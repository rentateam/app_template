<?php
namespace Project\Module\AdminLogModule\LogInternal;

use Project\Module\AdminLogModule\LogInternal\LogDecorator\LogDecoratorInterface;

class LogDecorator
{
    protected $decorators = [];

    /**
     * @param LogDecoratorInterface $logDecorator
     */
    public function registerLogDecorator(LogDecoratorInterface $logDecorator)
    {
        $this->decorators[] = $logDecorator;
    }

    /**
     * @param array $current
     * @return null|LogEntry\AccountLogEntry
     */
    public function apply($current)
    {
        $log = null;

        /** @var LogDecoratorInterface $decorator */
        foreach ($this->decorators as $decorator) {
            if ($decorator->isValid($current)) {
                $log = $decorator->decorateLog($current);
            }
        }

        return $log;
    }
}