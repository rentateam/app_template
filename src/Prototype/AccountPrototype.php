<?php
namespace Prototype;

use Constraint\AccountEmailUnique;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class AccountPrototype
{
    protected $id;
    protected $name;
    protected $email;
    protected $password;
    protected $accountRoleId;

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getAccountRoleId()
    {
        return $this->accountRoleId;
    }

    /**
     * @param int $accountRoleId
     */
    public function setAccountRoleId($accountRoleId)
    {
        $this->accountRoleId = $accountRoleId;
    }

    /**
     * @inheritdoc
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new NotBlank(['groups' => ['create', 'edit']]))
                 ->addPropertyConstraint('name', new Length(['groups' => ['create', 'edit'], 'min' => 2, 'max' => 255]));

        $metadata->addPropertyConstraint('email', new NotBlank(['groups' => ['create', 'edit']]))
                 ->addPropertyConstraint('email', new Length(['groups' => ['create', 'edit'], 'min' => 2, 'max' => 255]))
                 ->addPropertyConstraint('email', new Email(['groups' => ['create', 'edit']]))
                 ->addPropertyConstraint('email', new AccountEmailUnique(['groups' => ['create', 'edit']]));

        $metadata->addPropertyConstraint('password', new NotBlank(['groups' => ['create', 'change_password']]))
                 ->addPropertyConstraint('password', new Length(['groups' => ['create', 'change_password'], 'min' => 2, 'max' => 255]));

        $metadata->addPropertyConstraint('accountRoleId', new NotBlank(['groups' => ['edit']]));

    }
}