<?php
namespace QueryBuilder;

use Criteria\AbstractCriteria;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

abstract class AbstractQueryBuilder
{
    protected $sql;

    /**
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->sql = new Sql($adapter);
    }

    /**
     * @param AbstractCriteria $criteria
     * @return \Zend\Db\Adapter\Driver\StatementInterface
     * @throws \Exception
     */
    public function getStatement(AbstractCriteria $criteria)
    {
        if (false === $this->isValidCriteria($criteria)) {
            throw new \Exception("Criteria class mismatch");
        }

        $select = $this->createSelect($criteria);
        return $this->sql->prepareStatementForSqlObject($select);
    }

    /**
     * @param AbstractCriteria $criteria
     * @return Select
     */
    public abstract function createSelect(AbstractCriteria $criteria);

    /**
     * @param AbstractCriteria $criteria
     * @return bool
     */
    public abstract function isValidCriteria(AbstractCriteria $criteria);
}