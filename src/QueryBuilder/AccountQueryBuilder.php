<?php
namespace QueryBuilder;

use Criteria\AccountCriteria;
use Criteria\AbstractCriteria;
use Zend\Db\Sql\Select;

class AccountQueryBuilder extends AbstractQueryBuilder
{
    /**
     * @param AbstractCriteria $criteria
     * @return bool
     */
    public function isValidCriteria(AbstractCriteria $criteria)
    {
        return $criteria instanceof AccountCriteria;
    }

    /**
     * @param AbstractCriteria $criteria
     * @return Select
     */
    public function createSelect(AbstractCriteria $criteria)
    {
        $select = $this->sql->select()
            ->from(['a' => 'Account'])
            ->columns(['id']);

        return $select;
    }
}