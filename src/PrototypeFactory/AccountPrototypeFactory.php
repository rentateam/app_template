<?php
namespace PrototypeFactory;

use Prototype\AccountPrototype;

class AccountPrototypeFactory
{
    /**
     * @param \Entity_Account|null $account
     * @return AccountPrototype
     */
    public function createPrototype(\Entity_Account $account = null)
    {
        $proto = new AccountPrototype();
        if ($account instanceof \Entity_Account) {
            $proto->setId($account->getId());
            $proto->setEmail($account->getEmail());
            $proto->setName($account->getName());
            $proto->setAccountRoleId($account->getAccountRoleId());
        }

        return $proto;
    }
}