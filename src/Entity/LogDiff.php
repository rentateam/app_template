<?php

use SFM\Entity;

class Entity_LogDiff extends Entity
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLogDiffSetId()
    {
        return $this->log_diffset_id;
    }

    /**
     * @return Entity_LogDiffSet
     */
    public function getLogDiffSet()
    {
        return $this->mapper->getRepository()->get('Mapper_LogDiffSet')->getEntityById($this->getLogDiffSetId());
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getBefore()
    {
        return $this->before;
    }

    /**
     * @return string
     */
    public function getAfter()
    {
        return $this->after;
    }
}