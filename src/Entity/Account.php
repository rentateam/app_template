<?php

use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

class Entity_Account extends \SFM\Entity implements UserInterface
{
    /**
     * @return bool
     */
    public function isDeleted()
    {
        return (bool) $this->is_deleted;
    }

    /**
     * @return int
     */
    public function getAccountRoleId()
    {
        return (int)$this->account_role_id;
    }

    /**
     * Returns the roles granted to the user.
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        return array($this->getAccountRole()->getCode());
    }

    /**
     * @return Entity_AccountRole
     */
    public function getAccountRole()
    {
        return $this->mapper->getRepository()->get("Mapper_AccountRole")->getEntityById($this->account_role_id);
    }

    /**
     * Returns the password used to authenticate the user.
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     * This can return null if the password was not encoded using a salt.
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Removes sensitive data from the user.
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {

    }

    /**
     * Returns the string identifier of the Role
     * @return string
     */
    public function getRoleId()
    {
        return $this->getId();
    }
}