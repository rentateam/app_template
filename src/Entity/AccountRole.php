<?php
class Entity_AccountRole extends \SFM\Entity
{
    /**
     *   @return int
     **/
    public function getId()
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }
}