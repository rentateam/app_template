<?php

use SFM\Entity;

class Entity_LogAction extends Entity
{
    const ID_ACCOUNT_CREATED = 1;
    const ID_ACCOUNT_UPDATED = 2;
    const ID_ACCOUNT_PASSWORD_CHANGED = 3;
    const ID_ACCOUNT_DELETED = 4;
    const ID_ACCOUNT_RESTORED = 5;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}