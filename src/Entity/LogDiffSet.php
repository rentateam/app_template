<?php

use SFM\Entity;

class Entity_LogDiffSet extends Entity
{
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Aggregate_LogDiff
     */
    public function getLogDiffList()
    {
        return $this->mapper->getRepository()->get('Mapper_LogDiff')->getLogDiffList($this);
    }
}