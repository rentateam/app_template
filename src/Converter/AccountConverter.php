<?php
namespace Converter;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccountConverter
{
    protected $mapper;

    /**
     * @param \Mapper_Account $mapper
     */
    public function __construct(\Mapper_Account $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param int $id
     * @return \Entity_Account
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function convert($id)
    {
        $branch = $this->mapper->getEntityById($id);
        if (null === $branch) {
            throw new NotFoundHttpException();
        }

        return $branch;
    }
}