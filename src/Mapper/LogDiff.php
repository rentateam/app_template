<?php

use SFM\Manager;
use SFM\Mapper;
use Zend\Db\Sql\Sql;

class Mapper_LogDiff extends Mapper
{
    protected $tableName = 'LogDiff';

    /**
     * @param Entity_LogDiffSet $diffSet
     * @return Aggregate_LogDiff
     */
    public function getLogDiffList(Entity_LogDiffSet $diffSet)
    {
        $sql = new Sql(Manager::getInstance()->getDb()->getAdapter());
        $select = $sql->select()
            ->from('LogDiff')
            ->where(['log_diffset_id' => $diffSet->getId()]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $aggregate = $this->getAggregateBySQL($statement->getSql(), $statement->getParameterContainer()->getNamedArray());

        return $aggregate;
    }
}