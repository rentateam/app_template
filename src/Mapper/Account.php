<?php
use Criteria\AccountCriteria;
use QueryBuilder\AccountQueryBuilder;
use SFM\Manager;

class Mapper_Account extends \SFM\Mapper
{
    protected $tableName = 'Account';

    protected $uniqueFields = array(
        array('email')
    );

    /**
     * @param string $username
     * @return Entity_Account|null
     */
    public function getEntityByUsername($username)
    {
        /** @var Entity_Account $account */
        $account = $this->getEntityByUniqueFields(array('email' => $username));
        return ($account && $account->isDeleted()) ? null : $account;
    }

    /**
     * @param Entity_Account $account
     * @param string $password
     */
    public function setPassword(Entity_Account $account, $password)
    {
        $this->updateEntity(array('password' => $password), $account);
    }

    /**
     * @param AccountCriteria $criteria
     * @return Aggregate_Account
     * @throws Exception
     */
    public function getList(AccountCriteria $criteria)
    {
        $builder = new AccountQueryBuilder(Manager::getInstance()->getDb()->getAdapter());
        $statement = $builder->getStatement($criteria);

        return $this->getAggregateBySQL($statement->getSql(), $statement->getParameterContainer()->getNamedArray());
    }
}