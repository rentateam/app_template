<?php
/**
 * @method Entity_AccountRole getEntityById() getEntityById(int $id)
 */
class Mapper_AccountRole extends \SFM\Mapper
{
    protected $tableName = 'AccountRole';

    /**
     * @return array
     */
    public function getHierarchies()
    {
        return \SFM\Manager::getInstance()->getDb()->fetchAll("
            SELECT `r`.`code` AS `role_code`, `p`.`code` as `privilege_code`
            FROM `AccountRoleToPrivilege` AS `link`
            LEFT JOIN `AccountRole` AS `r` ON `r`.`id` = `link`.`account_role_id`
            LEFT JOIN `Privilege` AS `p` ON `p`.`id` = `link`.`privilege_id`
        ");
    }
}