<?php
namespace Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AccountEmailUniqueValidator extends ConstraintValidator
{
    protected $mapperAccount;

    /**
     * @param \Mapper_Account $mapperAccount
     */
    public function __construct(\Mapper_Account $mapperAccount)
    {
        $this->mapperAccount = $mapperAccount;
    }

    /**
     * Checks if the passed value is valid.
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        $entity = $this->mapperAccount->getEntityByUsername($value);

        // creation
        if ($this->context->getRoot()->get('id')->getData() === null) {
            if ($entity instanceof \Entity_Account) {
                $this->context->addViolation($constraint->message,  array('%string%' => $value));
            }
            // edit
        } else {
            if ($entity instanceof \Entity_Account && $entity->getId() != $this->context->getRoot()->get('id')->getData()) {
                $this->context->addViolation($constraint->message,  array('%string%' => $value));
            }
        }
    }
}