<?php
namespace Constraint;

use Symfony\Component\Validator\Constraint;

class AccountEmailUnique extends Constraint
{
    public $message = 'Account email must be unique';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'validator.account_email_unique';
    }
}