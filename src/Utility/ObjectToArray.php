<?php
namespace Utility;

class ObjectToArray
{
    /**
     * @param object $object
     * @return array
     */
    public function toArray($object)
    {
        $array = [];
        /** @var \ReflectionProperty $reflectionProperty */
        foreach ((new \ReflectionObject($object))->getProperties() as $reflectionProperty) {
            $reflectionProperty->setAccessible(true);
            $array[$reflectionProperty->getName()] = $reflectionProperty->getValue($object);
        }

        return $array;
    }
}