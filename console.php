<?php
use Project\Application;

set_time_limit(0);
chdir(__DIR__);

/** @var Application $app */
$app = require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

/** @var \Symfony\Component\Console\Application $application */
$application = $app['console'];
$application->run();